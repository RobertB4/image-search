## Install dependencies

```
npm install
```

## Start development server

```
npm run dev
```

## Build production bundle

```
npm run build
```
