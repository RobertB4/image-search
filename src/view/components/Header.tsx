import React from "react";
import styled from "styled-components";

import { SearchBar } from "@components/SearchBar";

export const Header = (): JSX.Element => {
  return (
    <Container>
      <ContainerInner>
        <div>Unsplash</div>
        <SearchBar />
      </ContainerInner>
    </Container>
  );
};

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 60px;
  padding: 0 16px;
  background-color: #ffffff;
  box-shadow: 4px 4px 4px 4px #f1f1f1;
  z-index: 100;
`;

const ContainerInner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: inherit;
  max-width: 1280px;
  margin: 0 auto;
`;
