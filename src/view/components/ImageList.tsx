import React from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";

import { imagesSelector } from "@store/images/selectors";

export const ImageList = (): JSX.Element => {
  const images = useSelector(imagesSelector);

  if (images.length === 0) {
    return (
      <Container>
        <ContainerInner>No images to display.</ContainerInner>
      </Container>
    );
  }

  return (
    <Container>
      <ContainerInner>
        <Images>
          {images.map(image => (
            <Img src={image.url} key={image.id} />
          ))}
        </Images>
      </ContainerInner>
    </Container>
  );
};

const Container = styled.div`
  padding: 0 16px;
`;

const ContainerInner = styled.div`
  margin: 96px auto 0 auto;
  max-width: 1280px;
`;

const Images = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

const Img = styled.img`
  width: 32.33%;
  max-height: 350px;
  margin-bottom: 16px;

  margin-right: 1%;
  :nth-child(3n) {
    margin-right: 0px;
  }
`;
