import React, { useState } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";

import { searchImages } from "@store/images/gateways";

export const SearchBar = (): JSX.Element => {
  const [searchTerm, setSearchTerm] = useState("");

  const dispatch = useDispatch();

  const handleSetSearchTerm = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(e.target.value);
  };

  const handleBlur = (e: React.FocusEvent<HTMLInputElement>) => {
    dispatch(searchImages(e.target.value));
  };

  return (
    <Input
      type="text"
      value={searchTerm}
      onChange={handleSetSearchTerm}
      onBlur={handleBlur}
      placeholder="Search for Images..."
    />
  );
};

const Input = styled.input`
  width: 100%
  height: 36px;
  border: 1px solid #eceff1;
  padding: 12px;
  border-radius: 20px;
  font-size: 14px;
  background-color: #eceff1;

  &:focus {
    border: 1px solid #e8f5e9;
  }
`;
