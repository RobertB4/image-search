import React from "react";

import { Header } from "@components/Header";
import { ImageList } from "@components/ImageList";

export const SearchResults = (): JSX.Element => (
  <>
    <Header />
    <ImageList />
  </>
);
