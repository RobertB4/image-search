import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";

import { ResetCss, GlobalStyles } from "./globalStyles";
import { store } from "@store/index";
import { SearchResults } from "@pages/SearchResults";

const Root = () => (
  <Provider store={store}>
    <ResetCss />
    <GlobalStyles />
    <SearchResults />
  </Provider>
);

render(<Root />, document.getElementById("page"));
