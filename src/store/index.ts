import { configureStore, ThunkAction } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import { createApi } from "unsplash-js";

import { imagesSlice, ImagesState } from "@store/images/slice";

export type RootState = {
  images: ImagesState;
};

export type RootAction<Payload = any> = { type: string; payload?: Payload };

export type RootThunkAction<ReturnType, Payload = any> = ThunkAction<
  ReturnType,
  RootState,
  typeof unsplash,
  RootAction<Payload>
>;

const unsplash = createApi({
  accessKey: "vIh06bizeQYheKuV6xuL3GBgV0of59egW0owj4mW984",
  headers: { "Content-Type": "application/json" }
});

export const store = configureStore<RootState, RootAction>({
  reducer: {
    images: imagesSlice.reducer
  },
  middleware: [thunk.withExtraArgument(unsplash)]
});
