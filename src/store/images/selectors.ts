import { Selector } from "react-redux";

import { RootState } from "@store/index";

export const imagesSelector: Selector<RootState, { id: string; url: string }[]> = state => state.images.data;
