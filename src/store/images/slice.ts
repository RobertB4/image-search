import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Image } from "@entities/image";

export type ImagesState = {
  data: Image[];
};

const initialState: ImagesState = {
  data: []
};

export const imagesSlice = createSlice({
  name: "images",
  initialState,
  reducers: {
    populate: (state, action: PayloadAction<{ images: Image[] }>) => {
      state.data = action.payload.images;
    }
  }
});
