import { RootThunkAction } from "@store/index";
import { imagesSlice } from "@store/images/slice";

export const searchImages = (searchTerm: string): RootThunkAction<Promise<void>> => async (
  dispatch,
  _,
  api
) => {
  const res = await api.search.getPhotos({
    query: searchTerm,
    orientation: "squarish"
  });

  const images =
    res.response?.results.map(image => ({
      id: image.id,
      url: image.urls.regular
    })) ?? [];

  dispatch(imagesSlice.actions.populate({ images }));
};
